function V = generacjaMacierzyPodobienstwaLokal(gen1,gen2,MacierzSub,gap)
 gen1=['-',gen1];
 gen2=['-',gen2];
 V = zeros(length(gen1),length(gen2));
 wartosciPomocnicze=[];
for iWymiar1=2:1:length(gen1)
   for jWymiar2=2:1:length(gen2)
       
      for m = 2:1:size(MacierzSub,2)
           if MacierzSub{1,m} == gen1(iWymiar1)
              for n = 2:1:size(MacierzSub,1)
                 if MacierzSub{n,1}==gen2(jWymiar2)
                     wartosciPomocnicze(1)= V(iWymiar1-1,jWymiar2-1)+MacierzSub{m,n};
                 end
              end
           end
      end
      
      wartosciPomocnicze(2)=V(iWymiar1-1,jWymiar2)+gap;
      wartosciPomocnicze(3)=V(iWymiar1,jWymiar2-1)+gap;
      wartosciPomocnicze(4)=0;
      V(iWymiar1,jWymiar2)=max(wartosciPomocnicze);
   end
end
end